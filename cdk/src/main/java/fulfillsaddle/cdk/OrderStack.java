package order.cdk;

import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.core.StackProps;

public class OrderStack extends Stack {

    public OrderStack(final Construct scope, final String id) {
        this(scope, id, null);
    }

    public OrderStack(final Construct scope, final String id, final StackProps props) {
        super(scope, id, props);

        /* Define the FulfillGriffin ECS service: */


		/* Define the service's MySQL database: */


    }
}
