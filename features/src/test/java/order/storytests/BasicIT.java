package order.storytests;

import order.stub.*;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.net.URL;

public class BasicIT {

	private int instance_orderId = 0;

	private String orderFactoryClassName = System.getenv("ORDER_FACTORY_CLASS_NAME");
	private OrderFactory oFac;
	private OrderStub order;

	private String jdbcPort = System.getenv("ORDER_MYSQL_CONTAINER_PORT");
	private String jdbcUrl = "jdbc:mysql://localhost:" + this.jdbcPort + "/Orders?connectTimeout=0&socketTimeout=0&autoReconnect=true";
	private String jdbcUser = System.getenv("ORDER_MYSQL_USER");
	private String jdbcPass = System.getenv("ORDER_MYSQL_PASSWORD");

	public BasicIT() {
		if (orderFactoryClassName == null) throw new Error(
			"Env variable ORDER_FACTORY_CLASS_NAME not set");
		try {
			oFac = (OrderFactory)(Class.forName(orderFactoryClassName).getDeclaredConstructor().newInstance());
			order = oFac.createOrder();
		} catch (Exception ex) {
			throw new RuntimeException("During startup", ex);
		}
		System.err.println("Constructed Order stub");

		if (this.jdbcPort == null) throw new RuntimeException("Env variable ORDER_MYSQL_CONTAINER_PORT not set");
		if (this.jdbcUser == null) throw new RuntimeException("Env variable ORDER_MYSQL_USER not set");
		if (this.jdbcPass == null) throw new RuntimeException("Env variable ORDER_MYSQL_PASSWORD not set");
	}

	/* Scenario */

	@When("I call Provision with department number {int}")
	public void call_place_order(int deptNo) throws Exception {
		this.instance_orderId = order.provision(deptNo);
	}

	@Then("it returns an order ID")
	public void returns_an_order_id() throws Exception {
		if (this.instance_orderId == 0) throw new Exception("No order Id was returned");
	}

	@Given("I called Provision with department number {int}")
	public void call_provision(int deptNo) throws Exception {
		this.instance_orderId = order.provision(deptNo);
	}

	@When("I call CancelOrder on the order")
	public void call_cancel_order() throws Exception {
		order.cancelOrder(this.instance_orderId);
	}

	@Then("it does not return an error")
	public void does_not_return_an_error() throws Exception {
	}

	private void purgeDb() throws Exception {
		try {
			System.err.println("Getting connection to database...");
			Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
			if (conn == null) throw new Exception("Unable to connect to Orders database");
			Statement stmt = conn.createStatement();
			System.err.println("About to truncate Orders...");
			stmt.executeUpdate("TRUNCATE TABLE Orders");
			System.err.println("...truncated Orders.");
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}
	}
}
