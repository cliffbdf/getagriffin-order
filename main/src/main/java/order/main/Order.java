package order.main;

import static spark.Spark.*;
import com.google.gson.Gson;

import fulfillgriffin.stub.*;
import fulfillsaddle.stub.*;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Order {

	private FulfillGriffinStub fg;
	private FulfillSaddleStub fs;
	private String jdbcPort;
	private String jdbcUrl;
	private String jdbcUser;
	private String jdbcPass;
	private Logger logger = LoggerFactory.getLogger(Order.class);

	/** The main method starts this microservice. */
	public static void main(String[] args) {

		/* These factories should be set depending on whether this the other components
		are mocked, in which case the factories create mock stubs that make local calls,
		or live instances, in which case the factories return live stubs that make
		remote calls. */
		String fulfillGriffinFactoryClassName = System.getenv("FULFILL_GRIFFIN_FACTORY_CLASS_NAME");
		String fulfillSaddleFactoryClassName = System.getenv("FULFILL_SADDLE_FACTORY_CLASS_NAME");
		FulfillGriffinFactory fgFac;
		FulfillSaddleFactory fsFac;
		FulfillGriffinStub fg;
		FulfillSaddleStub fs;
		try {
			fgFac = (FulfillGriffinFactory)(Class.forName(fulfillGriffinFactoryClassName).getDeclaredConstructor().newInstance());
			fsFac = (FulfillSaddleFactory)(Class.forName(fulfillSaddleFactoryClassName).getDeclaredConstructor().newInstance());
			fg = fgFac.createFulfillGriffin();
			fs =fsFac.createFulfillSaddle();
		} catch (Exception ex) {
			throw new RuntimeException("During startup", ex);
		}

		/* Instantiate the object that implements this service's logic. */
		Order order = new Order(fg, fs);
		Logger logger = order.logger;

		// https://docs.oracle.com/javase/tutorial/java/generics/genTypeInference.html
		get("/Provision", (req, res) -> {

			try {
				logger.info("Received request Provision...");
				String deptNoStr = req.queryParams("DeptNo");
				if (deptNoStr == null) {
					throw new Exception("DeptNo parameter missing");
				}
				int deptNo = 0;
				try { deptNo = Integer.parseInt(deptNoStr); }
				catch (Exception ex) {
					throw new Exception("DeptNo parameter must be an int");
				}

				int orderId = order.placeOrder(deptNo);
				OrderResponse response = new OrderResponse(orderId);

				Gson gson = new Gson();
	 			String json = json = gson.toJson(response);

				return json.toString();

			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		get("/CancelOrder", (req, res) -> {
			try {
				logger.info("Received request CancelOrder...");
				String orderIdStr = req.queryParams("OrderId");
				if (orderIdStr == null) {
					throw new Exception("OrderId parameter missing");
				}
				logger.info("A");
				int orderId = 0;
				try { orderId = Integer.parseInt(orderIdStr); }
				catch (Exception ex) {
					throw new Exception("OrderId parameter must be an int");
				}
				logger.info("B");

				order.cancelOrder(orderId);
				logger.info("C");
				return "order cancelled";

			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});

		get("/ping", (req, res) -> {
			try {
				logger.info("Received ping...");
				return "ping";
			} catch (RuntimeException re) {
				logger.debug(re.getMessage(), re);
				res.status(500);
				String msg = re.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
				res.status(400);
				String msg = ex.getMessage();
				if (msg == null) return "No message";
				return msg;
			} catch (Throwable t) {
				logger.debug(t.getMessage(), t);
				res.status(500);
				String msg = t.getMessage();
				if (msg == null) return "No message";
				return msg;
			}
		});
	}

	static class OrderResponse {
		public int orderId;
		public OrderResponse(int orderId) { this.orderId = orderId; }
		public void setOrderId(int orderId) { this.orderId = orderId; }
		public int getOrderId() { return this.orderId; }
	}

	private Order(FulfillGriffinStub fg, FulfillSaddleStub fs) {
		this.fg = fg;
		this.fs = fs;

		this.jdbcPort = System.getenv("ORDER_MYSQL_CONTAINER_PORT");
		this.jdbcUrl = "jdbc:mysql://odb:" + this.jdbcPort + "/Orders?connectTimeout=0&socketTimeout=0&autoReconnect=true";
		this.jdbcUser = System.getenv("MYSQL_USER");
		this.jdbcPass = System.getenv("MYSQL_PASSWORD");

		if (this.jdbcUrl == null) throw new RuntimeException("Env variable ORDER_MYSQL_CONTAINER_PORT not set");
		if (this.jdbcUser == null) throw new RuntimeException("Env variable MYSQL_USER not set");
		if (this.jdbcPass == null) throw new RuntimeException("Env variable MYSQL_PASSWORD not set");
	}

	/*
	Our database table needs to have these fields:
		OrderId
		DeptNo
		GriffinId
		SaddleId
	*/

	/** Place an order for a griffin, and return a unique order ID. */
	private int placeOrder(int deptNo) throws Exception {

		/* Call the remote FulfillGriffin service. */
		String griffinId = this.fg.requisitionFromInventory(deptNo);
		// Note: the above could fail....

		/* Call the remote FulfillSaddle service. */
		String saddleId;
		try {
			saddleId = this.fs.requisitionFromInventory(deptNo);
	 	} catch (Exception ex) {
			/* Cancel the griffin requisition. */
			try {
				this.fg.cancelRequisition(griffinId);
			} catch (Exception ex2) {
				throw new Exception(
					"Saddle requisition failed, but unable to cancel griffin requisition", ex2);
			}
			throw new Exception("Saddle requisition failed, so order aborted: " + ex.getMessage());
		}

		/* Create a new Order record. */
		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
		if (conn == null) throw new Exception("Unable to connect to Orders database");
		conn.setAutoCommit(false);

		int orderId = 0;
		try {
			/* Find a table row whose "DeptNo" field is empty. */
			Statement stmt1 = conn.createStatement();
			stmt1.executeUpdate(
				"INSERT INTO Orders (DeptNo, GriffinId, SaddleId) " +
				"VALUES (" + deptNo + ", '" + griffinId + "', '" + saddleId + "')");


			Statement stmt2 = conn.createStatement();
			ResultSet resultSet = stmt2.executeQuery("SELECT LAST_INSERT_ID() LIMIT 1");

			for (; resultSet.next();) {
				orderId = resultSet.getInt(1);
			}
			if (orderId == 0) throw new RuntimeException("Zero returned for order Id");

			conn.commit();

		} catch (Exception ex) {
			conn.rollback();
			/* We need to undo both requisitions. */

			String message = "Order failed: " + ex.getMessage();
			try {
				this.fg.cancelRequisition(griffinId);
			} catch (Exception ex2) {
				message += "; unable to cancel griffin requisition: " + ex2.getMessage();
			}
			try {
				this.fs.cancelRequisition(saddleId);
			} catch (Exception ex3) {
				message += "; unable to cancel saddle requisition: " + ex3.getMessage();
			}

			throw new Exception(message);
		} finally {
			conn.close();
		}

		return orderId;
	}

	/** Cancel the order identified by the order ID. */
	private void cancelOrder(int orderId) throws Exception {

		/* For scaling, this would use a connection pool, so that
		connections are not shared by different calls to this method - enabling this method
		to be reentrant. */
		Connection conn = DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPass);
		if (conn == null) throw new Exception("Unable to connect to Orders database");

		try {
			/* Delete the row corresponding to the order Id. */
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("DELETE FROM Orders WHERE OrderId = " + orderId);

		} catch (Exception ex) {
			conn.rollback();
			throw ex;
		} finally {
			conn.close();
		}
	}
}
