package order.stub;
/* */
public interface OrderFactory {
	OrderStub createOrder() throws Exception;
}
