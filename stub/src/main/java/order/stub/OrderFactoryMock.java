package order.stub;

import java.util.Map;
import java.util.HashMap;

public class OrderFactoryMock implements OrderFactory {

	public OrderStub createOrder() throws Exception {
		return new OrderStub() {

			public int provision(int deptNo) throws Exception {
				throw new Exception("provision mock not implemented yet");
			}

			public void cancelOrder(int orderId) throws Exception {
				throw new Exception("cancelOrder mock not implemented yet");
			}

			public String ping() throws Exception {
				return "ping";
			}
		};
	}
}
